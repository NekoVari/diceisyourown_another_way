#include <iostream>

int B(int a[6]){
    int temp1;
    //0->3 1->0 5->1 3->5      0->3->5->1->0
    //
    temp1=a[0];
    a[0]=a[1];
    a[1]=a[5];
    a[5]=a[3];
    a[3]=temp1;
    return 0;
}
int F(int a[6]){//
    int temp1;
    //0->1 1->5 5->3 3->0
    temp1=a[0];
    a[0]=a[3];
    a[3]=a[5];
    a[5]=a[1];
    a[1]=temp1;
    return 0;
}
int L(int a[6]){
    int temp1;
    //0->2 2->5 4->0 5->4     0->2->5->4->0
    temp1=a[0];
    a[0]=a[4];
    a[4]=a[5];
    a[5]=a[2];
    a[2]=temp1;
    return 0;
}
int R(int a[6]){
    int temp1;
    //0->4 2->0 4->5 5->2  0->4->5->2->0
    temp1=a[0];
    a[0]=a[2];
    a[2]=a[5];
    a[5]=a[4];
    a[4]=temp1;
    return 0;
}
int C(int a[6]){
    int temp1;
    //1->2 2->3 3->4 4->1
    temp1=a[1];
    a[1]=a[4];
    a[4]=a[3];
    a[3]=a[2];
    a[2]=temp1;
    return 0;
}
int D(int a[6]){
    int temp1;
    //1->4 2->1 3->2 4->3  1->4->3->2->1
    temp1=a[1];
    a[1]=a[2];
    a[2]=a[3];
    a[3]=a[4];
    a[4]=temp1;
    return 0;
}
//บน หน้า ซ้าย หลัง ขวา ล่าง
int main() {
    int a[6]={1,2,3,5,4,6},i=0;
    char inp[50];
    printf("Input Keywords (F,B,L,R,C,D) : ");
    scanf("%s",inp);
    while(inp[i]!='\0'){
        if(inp[i]=='F'){
            F(a);
        }
        else if(inp[i]=='B'){
            B(a);
        }
        else if(inp[i]=='L'){
            L(a);
        }
        else if(inp[i]=='R'){
            R(a);
        }
        else if(inp[i]=='C'){
            C(a);
        }
        else if(inp[i]=='D'){
            D(a);
        }
        i++;
    }
    printf("Front side is : %d",a[1]);
    return 0;
}
